# Séance 12: Autotools & CMake

Autotools et CMake sont des outils permettant de gérer de manière beaucoup plus
élaborée la préparation (compilation, *patch*, etc.) et la distribution
(interface entre le développeur et le distributeur) d'un projet.

## 1 - Autotools

Le terme "Autotools" désigne un ensemble d'outils appelés *Autoconf* et
*Automake*. Ces deux programmes sont les primitives principales de la suite
Autotools.

- **Autoconf**: première étape dans l'analyse du projet. Cette étape génère
  le script `configure` qui pourra ensuite (plus tard) être utilisé pour obtenir
  les fichiers `Makefile`.
  On y définit:
  - le nom du projet;
  - la version;
  - les dépendances (bibliothèques, programmes pré-installés, etc.);
  - les conditions de construction (*build* en ang.): différentes éventualités
    peuvent être prévues (selon la plateforme, les dépendances présentes sur le
    système, etc.) afin de modifier le comportement de construction. Par
    exemple, on peut (dés)activer des règles du Makefile résultant.
- **Automake**: deuxième étape de l'analyse du projet. Cette étape utilise les
  fichiers `Makefile.am` écrits par le développeur et génère des fichiers
  `Makefile.in`. Dans les fichiers `Makefile.am`, on y définit les règles:
  - de compilation;
  - d'installation;
  - de paquetage (tar, zip, etc.);
  - de *patch*;
  - etc.

La combinaison de ces deux outils génère finalement des fichiers `Makefile`
POSIX où plusieurs préoccupations sont gérées de façon abstraite. Entre autres,
l'installation, la compilation, la création de bibliothèques (statique,
dynamique) est pris en charge avec, en contre-partie, peu d'interventions par le
développeur.

#### Documentation utile

Les liens suivants sont un bon endroit où démarrer:

- https://autotools.io
- https://www.gnu.org/software/autoconf/manual/autoconf.html
- https://www.gnu.org/software/automake/manual/automake.html
- Exemple de base (TL;DR):
  http://smalltalk.gnu.org/blog/bonzinip/all-you-should-really-know-about-autoconf-and-automake
- Tutoriel recommandé: https://www.dwheeler.com/autotools/

La documentation sur les Makefile peut s'avérer utile!

- https://www.gnu.org/software/make/manual/make.html


#### Objectif

Convertir la configuration à base de fichiers `Makefile` du [TP2][tp-cible] en
une configuration à base d'Autotools.

[tp-cible]: https://gitlab.com/ablondin/inf3135-aut2017-tp2

### 1.1 - Écrire le fichier `configure.ac`

Le fichier `configure.ac` contient le code analysé par `autoconf`. C'est le
premier fichier à écrire. Un exemple minimal est le suivant:

```config
AC_INIT([package], [version])
AM_INIT_AUTOMAKE([foreign subdir-objects])
AC_CONFIG_SRCDIR([configure.ac])
AC_CONFIG_HEADERS([config.h])     # not even really needed
AC_PROG_CC                        # or AC_PROG_CXX
AC_CONFIG_FILES([Makefile])
AC_OUTPUT
```

### 1.2 - Écrire le fichier `Makefile.am`

Les fichiers `Makefile.am` peuvent être vu comme un `Makefile`. Ce que vous y
inscrivez sera littéralement copié dans le `Makefile` final, mais avec beaucoup
d'autre code généré automatiquement. Pour chaque fichier `Makefile` désiré (par
répertoire), on doit écrire un fichier `Makefile.am`. Un exemple minimal est le
suivant:

```automake
bin_PROGRAMS = hello
hello_SOURCES = hello.c
```

### 1.3 - Exécuter les outils

Afin de procéder, on commence premièrement par exécuter les outils préliminaires
qui compléteront la configuration du projet au fur et à mesure:

```bash
$ aclocal
$ autoheader
```

Une fois cela effectué, vous apercevrez quelques nouveaux fichiers. Ceux-ci ont
été générés en analysant votre fichier `configure.ac` et contiennent des
informations importantes pour le système. On doit maintenant lancer `automake`
afin qu'il génère les fichiers `Makefile.in`:

```bash
$ automake -a
```

```bash
$ autoconf
```

On a maintenant le fichier `configure` qui a été produit. On peut l'exécuter:

```bash
./configure
```

Finalement, le fichier `Makefile` a été généré. On peut maintenant compiler le
projet:

```bash
$ make
```

#### Raccourci

Afin d'éviter à devoir déduire les commandes à appeler afin de reconfigurer le
dépôt au fur et à mesure que celui-ci change avec toutes ces commandes, il est
possible de simplement faire:

```bash
$ autoreconf -i
```

et toutes les étapes `aclocal`, `autoheader`, `automake` et `autoconf` seront
exécutées (si nécessaire) dans le bon ordre. En effet, depuis le début, il est
suffisant de simplement faire `autoreconf -i`. Il s'agit de la commande
normalement invoqué par tous (personne ne le fait manuellement au long).

**CEPENDANT**: il faut toujours exécuter le script `./configure`. En effet,
`autoreconf` ne réexécute que les parties autoconf et automake. Le script
`./configure` est une étape subséquente et toujours nécessaire.

### 1.4 - Ajouter les fichiers au dépôt

**TOUS LES FICHIERS NE DOIVENT PAS ÊTRE VERSIONNÉS**

Comme vous l'avez remarqué, plusieurs fichiers ont été générés et ceux-ci ne
doivent pas être versionnés car ils sont volatiles et sont toujours regénérés de
toute manière. Les fichiers à ajouter sont:

```
configure.ac
Makefile.am
```

#### Le contenu de `.gitignore`

Vous voudrez forcément bannir certains fichiers afin d'avoir la vue dégagée dans
la sortie de la commande `git status`. Ainsi, ajoutez les lignes suivantes à
votre fichier `.gitignore`:

```gitconfig
# autotools files
Makefile
Makefile.in
/aclocal.m4
/autom4te.cache/
/config.*
/configure
/depcomp
/install-sh
/libtool
/ltmain.sh
/missing
/stamp-h?
.deps/
.dirstamp
.libs/
*.l[ao]
*~
*.pc
```

Cette liste contient beaucoup de fichiers qui n'apparaissent pas dans une
configuration de base. Pour des projets plus complexe, cela vous sera utile.

### 1.5 - Ajouter le build en mode DEBUG

Ajouter le code suivant dans le fichier `configure.ac`.

```config
AC_ARG_ENABLE([debug], AS_HELP_STRING([--enable-debug], [Build in debug mode, adds stricter warnings, disables optimization]))
AS_IF([test "x$enable_debug" = "xyes"],
      [CXXFLAGS="${CXXFLAGS} -g -Wno-return-type -Wall -Wextra -Wnon-virtual-dtor -O0"],
      [CXXFLAGS="${CXXFLAGS} -O3"])
```

Reconfigurez autoconf et automake, puis exécutez le script `configure` avec la
nouvelle option `--enable-debug`. Une fois que c'est fait, les invocations de
`make` produiront une commande `gcc` avec l'option de *debug*:

```bash
-g -Wno-return-type -Wall -Wextra -Wnon-virtual-dtor -O0
```

## 2 - CMake

CMake est un autre programme lorsqu'exécuté sur une configuration donnée,
il produit un fichier de construction désiré. Les options de construction sont
appelées des *générateurs* (*generators* en ang.). CMake liste les générateurs
possibles sur leur site web:

> - **Borland Makefiles**: Generates Borland makefiles.
> - **MSYS Makefiles**: Generates MSYS makefiles. The makefiles use /bin/sh as
>   the shell. They require msys to be installed on the machine.
>
> - **MinGW Makefiles**: Generates a make file for use with mingw32-make.  The
>   makefiles generated use cmd.exe as the shell. They do not require msys or a
>   unix shell.
>
> - **NMake Makefiles**: Generates NMake makefiles.
> - **Unix Makefiles**: Generates standard UNIX makefiles. A hierarchy of UNIX
>   makefiles is generated into the build tree. Any standard UNIX-style make
>   program can build the project through the default make target.  A "make
>   install" target is also provided.
>
> - **Visual Studio 6**: Generates Visual Studio 6 project files.
> - **Visual Studio 7**: Generates Visual Studio .NET 2002 project files.
> - **Visual Studio 7 .NET 2003**: Generates Visual Studio .NET 2003 project files.
> - **Visual Studio 8 2005**: Generates Visual Studio .NET 2005 project files.
> - **Visual Studio 8 2005 Win64**: Generates Visual Studio .NET 2005 Win64 project files.
> - **Visual Studio 9 2008**: Generates Visual Studio 9 2008 project files.
> - **Visual Studio 9 2008 Win64**: Generates Visual Studio 9 2008 Win64 project files.
> - **Watcom WMake**: Generates Watcom WMake makefiles.

Dans le cadre du laboratoire, on ne s'intéresse qu'aux Makefiles style UNIX (le
comportement par défaut). Afin de configurer le projet, CMake nécessite
d'écrire le fichier `CMakeLists.txt`. Celui-ci contient toute la configuration
équivalente à ce que nous avons vu pour Autotools avec les fichiers
`configure.ac` et `Makefile.am`.

#### Documentation utile

- Documentation officielle en ligne https://cmake.org/cmake/help/v3.10/
- `man 1 cmake`
- `man 7 cmake-*`:

        cmake-buildsystem            cmake-generators  cmake-properties
        cmake-commands               cmake-language    cmake-qt
        cmake-compile-features       cmake-modules     cmake-server
        cmake-developer              cmake-packages    cmake-toolchains
        cmake-generator-expressions  cmake-policies    cmake-variables
- Tutoriel: http://preshing.com/20170511/how-to-build-a-cmake-based-project/

#### Objectif

Convertir la configuration à base de fichiers `Makefile` du [TP2][tp-cible] en
une configuration à base de CMake.

### 2.1 - Écrire le fichier `CMakeLists.txt`

Une configuration minimale ressemble à cela:

```cmake
cmake_minimum_required(VERSION 3.1)
project(toto)
set(toto_VERSION 0.3.3)

list(APPEND toto_SOURCES
    src/hello.c
)
add_executable(toto ${toto_SOURCES})
```

### 2.2 - Exécuter CMake

On commence premièrement par créer un répertoire `build` dans lequel CMake
pourra travailler:

```bash
$ mkdir -p build
```

Maintenant, à l'intérieur du répertoire nouvellement créé, on démarre CMake.
Afin de le faire rouler, on doit lui indiquer où se trouve le fichier
`CMakeLists.txt`. C'est le fichier qu'on a créé précédemment dans le répertoire
parent "..".

```bash
$ cd build && cmake ..
```

Maintenant, on peut utiliser le fichier `Makefile`.

## 3 - Réviser un ancien examen

Un [ancien examen](http://lacim.uqam.ca/~blondin/fr/inf3135) est disponible sur
le site du cours. N'hésitez pas à l'utiliser comme document de révision.
